package com.example.joustassignment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable, GestureDetector.OnGestureListener {
    final static String TAG = "JOUST-GAME";

    //screen height for canvas
    int screenHeight;
    int screenWidth;

    //game status
    Boolean isGameRunning;
    Boolean isGameOver = false;
    Boolean isWon = false;

    //thread for game play
    Thread gameThread;

    //UI helper objects to render the game
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    //object to detect the touch gestures
    private GestureDetector gestureDetector;

    //array to store different type of game object
    ArrayList<GameObject> enemies = new ArrayList<GameObject>();
    ArrayList<GameObject> hearts = new ArrayList<GameObject>();
    ArrayList<Integer> heartTimer = new ArrayList<Integer>();

    //varibale used in the game
    List<Bitmap> levels;
    GameObject enemy;
    GameObject player;
    GameObject playerHeight;
    GameObject heart;

    Integer newYPosition = 0;
    Integer heartX = 0;
    Integer heartY = 0;

    Boolean isPlayerUp = false;
    Boolean isPlayerDown = false;

    String PlayerTowards = "static";

    Integer playerLevel = 4;
    Integer enemyCount = 0;
    Integer lives = 2;
    Integer score = 0;

    public GameEngine(Context context, int width, int height) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = width;
        this.screenHeight = height;

        gestureDetector = new GestureDetector(getContext(), this);

        this.printScreenInfo();

        //setting images for different levels
        Bitmap level_1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.level_1);
        level_1 = Bitmap.createScaledBitmap(level_1, screenWidth, 80, false);

        Bitmap level_2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.level_2);
        level_2 = Bitmap.createScaledBitmap(level_2, screenWidth, 80, false);

        Bitmap level_3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.level_3);
        level_3 = Bitmap.createScaledBitmap(level_3, screenWidth, 80, false);

        Bitmap level_4 = BitmapFactory.decodeResource(context.getResources(), R.drawable.level_4);
        level_4 = Bitmap.createScaledBitmap(level_4, screenWidth, 80, false);

        this.levels = new ArrayList<Bitmap>();

        this.levels.add(level_1);
        this.levels.add(level_2);
        this.levels.add(level_3);
        this.levels.add(level_4);

        //creating game objects
        enemy = new GameObject(getContext(), 100, 200, R.drawable.enemy);
        playerHeight = new GameObject(getContext(), 0, 0, R.drawable.packman);
        this.player = new GameObject(getContext(), 50, (screenHeight - 400) - this.playerHeight.getImage().getHeight(), R.drawable.packman);
    }

    public void makeEnemy(int xs, int ys) {
        enemy = new GameObject(getContext(), xs, ys, R.drawable.enemy);
        enemies.add(enemy);
    }

    public void makeHeart(int xs, int ys) {
        heart = new GameObject(getContext(), xs, ys, R.drawable.heart);
        hearts.add(heart);
    }

    public int generateRandomLevel() {
        Random random = new Random();
        int level = random.nextInt(5);
        if (level == 1) {
            return screenHeight - 1600;
        } else if (level == 2) {
            return screenHeight - 1200;
        } else if (level == 3) {
            return screenHeight - 800;
        } else if (level == 4) {
            return screenHeight - 400;
        }
        return 0;
    }

    public int getNewYPosition(int level) {
        int newYPosition = screenHeight - 400 - this.playerHeight.getImage().getHeight();
        ;
        if (level == 1) {
            newYPosition = screenHeight - 1600 - this.playerHeight.getImage().getHeight();
        } else if (level == 2) {
            newYPosition = screenHeight - 1200 - this.playerHeight.getImage().getHeight();
        } else if (level == 3) {
            newYPosition = screenHeight - 800 - this.playerHeight.getImage().getHeight();
        } else if (level == 4) {
            newYPosition = screenHeight - 400 - this.playerHeight.getImage().getHeight();
        }
        return newYPosition;
    }

    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    @Override
    public void run() {
        while (isGameRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }

    public void pauseGame() {
        isGameRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startGame() {
        isGameRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void updatePositions() {
        if (PlayerTowards != "static") {
            if (PlayerTowards == "Right") {
                if (this.player.getxPosition() >= this.screenWidth) {
                    this.player.setxPosition((0 - this.player.getImage().getWidth()));
                }
                this.player.setxPosition(this.player.getxPosition() + 30);
                this.player.updateHitbox();
            } else if (PlayerTowards == "Left") {
                if ((this.player.getxPosition() + this.player.getImage().getWidth()) <= 0) {
                    this.player.setxPosition(this.screenWidth);
                }
                this.player.setxPosition(this.player.getxPosition() - 30);
                this.player.updateHitbox();
            } else if (PlayerTowards == "Down") {
                if (playerLevel != 4)
                    playerLevel++;
                else
                    playerLevel = 1;
                int level = this.playerLevel;
                PlayerTowards = "static";
                isPlayerDown = true;

                newYPosition = getNewYPosition(level);
            } else if (PlayerTowards == "Up") {
                if (playerLevel != 1)
                    playerLevel--;
                else
                    playerLevel = 4;

                int level = this.playerLevel;
                PlayerTowards = "static";
                isPlayerUp = true;
                newYPosition = getNewYPosition(level);
            }
        }

        if (this.player.getyPosition() != newYPosition && isPlayerUp == true) {
            this.player.setyPosition(this.player.getyPosition() - 230);
            if (this.player.getyPosition() <= newYPosition) {
                this.player.setyPosition(newYPosition);
                isPlayerUp = false;
            }
            this.player.updateHitbox();
        }

        if (this.player.getyPosition() != newYPosition && isPlayerDown == true) {
            if (this.player.getyPosition() <= newYPosition) {
                this.player.setyPosition(this.player.getyPosition() + 230);
            } else if (this.player.getyPosition() > newYPosition) {
                this.player.setyPosition(0);
                this.player.setyPosition(this.player.getyPosition() + 230);
            }
            if ((this.player.getyPosition() >= 0) && (this.player.getyPosition() >= newYPosition)) {
                this.player.setyPosition(newYPosition);
                isPlayerDown = false;
            }
            this.player.updateHitbox();
        }

        if (enemies.size() > 0 || hearts.size() > 0) {
            for (int i = 0; i < enemies.size(); i++) {
                GameObject gameObject = enemies.get(i);
                gameObject.setxPosition(gameObject.getxPosition() + 15);
                gameObject.updateHitbox();

                if (gameObject.getHitbox().intersect(player.getHitboxLeft())) {
                    if ((player.getyPosition() != screenHeight - 400 - this.playerHeight.getImage().getHeight()) || (player.getxPosition() != 50)) {
                        PlayerTowards = "Static";
                        playerLevel = 4;
                        isPlayerUp = false;
                        isPlayerDown = false;
                        player.setxPosition(50);
                        player.setyPosition(screenHeight - 400 - this.playerHeight.getImage().getHeight());
                        player.updateHitbox();
                        lives = lives - 1;
                    }
                }

                if (this.lives == 0 || this.enemyCount == 0) {
                    this.isGameOver = true;
                    return;
                }

                if (this.score > 3) {
                    this.isWon = true;
                    return;
                }

                if (gameObject.getHitbox().intersect(this.player.getHitboxRight())) {
                    heartX = gameObject.getxPosition();
                    heartY = gameObject.getyPosition();

                    makeHeart((int) ((Math.random() * (((this.screenWidth - this.enemy.image.getWidth()))))),
                            this.generateRandomLevel() - this.enemy.image.getHeight());

                    heartTimer.add((int) System.currentTimeMillis());
                    enemies.remove(gameObject);
                    score = score + 1;
                }
            }

            for (int i = 0; i < hearts.size(); i++) {
                if (player.getHitbox().intersect(hearts.get(i).getHitbox())) {
                    hearts.remove(i);
                    heartTimer.remove(i);
                    lives = lives + 1;
                }
            }

            for (int i = 0; i < hearts.size(); i++) {
                if ((int) System.currentTimeMillis() - heartTimer.get(i) > 10000) {
                    makeEnemy(hearts.get(i).getxPosition(), hearts.get(i).getyPosition());
                    hearts.remove(i);
                    heartTimer.remove(i);
                }
            }
        }
    }

    long currentTime = 0;
    long previousTime = 0;

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            Bitmap background = BitmapFactory.decodeResource(getResources(), R.drawable.background);
            background = Bitmap.createScaledBitmap(background, screenWidth, screenHeight, false);
            this.canvas.drawBitmap(background,0,0,null);

//            this.canvas.drawColor(Color.argb(255, 0, 0, 255));
            paintbrush.setColor(Color.WHITE);

            canvas.drawBitmap(levels.get(3), 0, screenHeight - 1600, paintbrush);
            canvas.drawBitmap(levels.get(2), 0, screenHeight - 1200, paintbrush);
            canvas.drawBitmap(levels.get(1), 0, screenHeight - 800, paintbrush);
            canvas.drawBitmap(levels.get(0), 0, screenHeight - 400, paintbrush);

            this.canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            currentTime = System.currentTimeMillis();
            if ((currentTime - previousTime) > 2000) {
                if (enemies.size() < 40) {
                    makeEnemy((int) ((Math.random() * (((this.screenWidth - this.enemy.image.getWidth()))))),
                            this.generateRandomLevel() - this.enemy.image.getHeight());
                    enemyCount++;
                }
                previousTime = currentTime;
            }
            if (enemies.size() > 0) {
                for (int i = 0; i < enemies.size(); i++) {
                    GameObject gameObject = enemies.get(i);
                    canvas.drawBitmap(gameObject.getImage(), gameObject.getxPosition(), gameObject.getyPosition(), paintbrush);
                    paintbrush.setColor(Color.WHITE);
                    paintbrush.setStyle(Paint.Style.STROKE);
                    paintbrush.setStrokeWidth(5);
                    gameObject.getHitbox();
                }
            }

            for (int i = 0; i < hearts.size(); i++) {
                GameObject egg = hearts.get(i);
                canvas.drawBitmap(egg.getImage(), egg.getxPosition(), egg.getyPosition(), paintbrush);
                paintbrush.setColor(Color.WHITE);
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setStrokeWidth(5);
                egg.getHitbox();
            }

            if (isGameOver == true) {
                canvas.drawText("GAME OVER!", screenWidth / 2, screenHeight / 2, paintbrush);
                Intent intent  =  new Intent(getContext(),ResultActivity.class);
                intent.putExtra("message","GAME OVER");
                getContext().startActivity(intent);

            }

            if (isWon == true) {
                canvas.drawText("YOU WON", screenWidth / 2, screenHeight / 2, paintbrush);
                Intent intent  =  new Intent(getContext(),ResultActivity.class);
                intent.putExtra("message","YOU WON");
                getContext().startActivity(intent);
            }

            paintbrush.setTextSize(60);
            paintbrush.setColor(Color.BLACK);
            paintbrush.setStyle(Paint.Style.STROKE);

            canvas.drawText(("Score: " + score), screenWidth - 300, 100, paintbrush);
            this.canvas.drawText(("Lives: " + lives), 20, 100, paintbrush);

            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(30);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        return gestureDetector.onTouchEvent(event);
    }


    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityInXAxis, float velocityInYAxis) {
        boolean result = false;
        //Minimum limit to check if the difference from a to b in touch gesture is more than 100dp
        final int SWIPE_MIN_LIMIT = 100;

        //Minimum limit to check if the velocity of the swipe is more than 100dp/second or not.
        final int SWIPE_VELOCITY_MIN_LIMIT = 100;
        try {
            float yAxisDifference = e2.getY() - e1.getY();    //Difference of dp in Y Axis
            float xAxisDifference = e2.getX() - e1.getX();    //Difference of dp in X Axis

            //If X Axis difference is more that means it's a left or right swipe else top or bottom swipe
            if (Math.abs(xAxisDifference) > Math.abs(yAxisDifference)) {

                //Mark as a left or right swipe if it's crosses the minimum limit value
                if (Math.abs(xAxisDifference) > SWIPE_MIN_LIMIT && Math.abs(velocityInXAxis) > SWIPE_VELOCITY_MIN_LIMIT) {

                        /*
                        If difference is in positive that means it's swipe from left to right else
                        swipe from right to left because x increase from right ot left and then
                        difference will be positive so call onSwipeRight method else onSwipeLeft method
                        */
                    if (xAxisDifference > 0) {
                        onSwipeRight();
                    } else {
                        onSwipeLeft();
                    }

                    //To check if we identified the swipe gesture
                    result = true;
                }
            } else if (Math.abs(yAxisDifference) > SWIPE_MIN_LIMIT && Math.abs(velocityInYAxis) > SWIPE_VELOCITY_MIN_LIMIT) {
                     /*
                        If difference is in positive that means it's swipe from top to bottom else
                        swipe from bottom to top because y increase from top ot bottom and then
                        difference will be positive so call onSwipeBottom method else onSwipeTop
                        */
                if (yAxisDifference > 0) {
                    onSwipeDown();
                } else {
                    onSwipeUp();
                }
                //To check if we identified the swipe gesture
                result = true;
            }
        } catch (Exception exception) {
            Log.d(TAG, "Exception occurred. Message: " + exception.getMessage());
            exception.printStackTrace();
        }

        //Return true if gesture is identified
        return result;
    }

    private void onSwipeLeft() {
        PlayerTowards = "Left";
    }

    private void onSwipeRight() {
        PlayerTowards = "Right";
    }

    private void onSwipeUp() {
        PlayerTowards = "Up";
    }

    private void onSwipeDown() {
        PlayerTowards = "Down";
    }
}