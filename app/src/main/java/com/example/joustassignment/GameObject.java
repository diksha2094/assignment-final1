package com.example.joustassignment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class GameObject {

    //position
    private int xPosition;
    private int yPosition;

    //game object image
    Bitmap image;

    //hit box rectangle
    Rect hitbox;

    public GameObject(Context context, int x, int y, int imageName) {
        this.xPosition = x;
        this.yPosition = y;

        this.image = BitmapFactory.decodeResource(context.getResources(), imageName);

        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.image.getWidth(),
                this.yPosition + this.image.getHeight()
        );
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public Rect getHitboxRight() {
        return new Rect(
                hitbox.left +  image.getWidth()/2,
                hitbox.top,
                hitbox.right,
                hitbox.bottom
        );
    }

    public Rect getHitboxLeft() {
        return new Rect(
                hitbox.left,
                hitbox.top,
                hitbox.right - image.getWidth()/2,
                hitbox.bottom
        );
    }

    public void updateHitbox() {
        this.hitbox.left = this.xPosition;
        this.hitbox.top = this.yPosition;
        this.hitbox.right = this.xPosition + this.image.getWidth();
        this.hitbox.bottom = this.yPosition + this.image.getHeight();
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}