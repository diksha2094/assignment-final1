package com.example.joustassignment;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {

    GameEngine gameEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //make it fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        gameEngine = new GameEngine(this, size.x, size.y);
        setContentView(gameEngine);
    }


    //lifecycle method to pause the game
    @Override
    protected void onPause() {
        super.onPause();
        gameEngine.pauseGame();
    }

    //lifecycle method to resume the game
    @Override
    protected void onResume() {
        super.onResume();
        gameEngine.startGame();
    }
}
