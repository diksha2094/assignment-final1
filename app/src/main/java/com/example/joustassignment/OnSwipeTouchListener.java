package com.example.joustassignment;


import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class OnSwipeTouchListener implements OnTouchListener {

    private final String DEB_TAG = "JOUST-GAME";

    //Class reference of the gestureDetector
    private final GestureDetector gestureDetector;

    //Parameterize constructor to initialize gesture detector with context of the activity who calls
    public OnSwipeTouchListener(Context ctx) {
        gestureDetector = new GestureDetector(ctx, new GestureListener());
    }

    //Override method of onTouchListener will return if touch is a gesture or not
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    //Class to check about type of gesture to identify the type of swipes
    private final class GestureListener extends SimpleOnGestureListener {

        //Minimum limit to check if the difference from a to b in touch gesture is more than 100dp
        private static final int SWIPE_MIN_LIMIT = 100;

        //Minimum limit to check if the velocity of the swipe is more than 100dp/second or not.
        private static final int SWIPE_VELOCITY_MIN_LIMIT = 100;

        //Check if it's under the motion/gesture
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        //On completion of a gesture check what type it is
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityInXAxis, float velocityInYAxis) {
            boolean result = false;
            try {
                float yAxisDifference = e2.getY() - e1.getY();    //Difference of dp in Y Axis
                float xAxisDifference = e2.getX() - e1.getX();    //Difference of dp in X Axis

                //If X Axis difference is more that means it's a left or right swipe else top or bottom swipe
                if (Math.abs(xAxisDifference) > Math.abs(yAxisDifference)) {

                    //Mark as a left or right swipe if it's crosses the minimum limit value
                    if (Math.abs(xAxisDifference) > SWIPE_MIN_LIMIT && Math.abs(velocityInXAxis) > SWIPE_VELOCITY_MIN_LIMIT) {

                        /*
                        If difference is in positive that means it's swipe from left to right else
                        swipe from right to left because x increase from right ot left and then
                        difference will be positive so call onSwipeRight method else onSwipeLeft method
                        */
                        if (xAxisDifference > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }

                        //To check if we identified the swipe gesture
                        result = true;
                    }
                } else if (Math.abs(yAxisDifference) > SWIPE_MIN_LIMIT && Math.abs(velocityInYAxis) > SWIPE_VELOCITY_MIN_LIMIT) {
                     /*
                        If difference is in positive that means it's swipe from top to bottom else
                        swipe from bottom to top because y increase from top ot bottom and then
                        difference will be positive so call onSwipeBottom method else onSwipeTop
                        */
                    if (yAxisDifference > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                    //To check if we identified the swipe gesture
                    result = true;
                }
            } catch (Exception exception) {
                Log.d(DEB_TAG,"Exception occurred. Message: " + exception.getMessage());
                exception.printStackTrace();
            }

            //Return true if gesture is identified
            return result;
        }
    }

    //Default methods to be defined in implementation class
    public void onSwipeRight() {
    }

    public void onSwipeLeft() {
    }

    public void onSwipeTop() {
    }

    public void onSwipeBottom() {
    }
}